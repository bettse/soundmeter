#!/usr/bin/python

import sys
import usb.core
import time
from telegraf.client import TelegrafClient

client = TelegrafClient(host = '10.0.1.200', port = 8092)
dev = usb.core.find(idVendor = 0x16c0, idProduct = 0x5dc)
assert dev is not None

print dev

while True:
    time.sleep(1)
    ret = dev.ctrl_transfer(0xC0, 4, 0, 0, 200)
    dB = (ret[0] + ((ret[1] & 3) * 256)) * 0.1 + 30
    client.metric('dB', dB, tags={})

